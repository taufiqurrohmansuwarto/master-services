const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class RwytDiklat extends Model {
  static get tableName() {
    return "siasn_master.rwyt_diklat";
  }

  static get idColumn() {
    return "siasn_master.rwyt_diklat.diklat_id";
  }

  static get relationMappings() {
    const refPeran = require("../siasn_ref/diklat_peran.model");
    const refSatuan = require("../siasn_ref/diklat_satuan.model");
    const refTingkat = require("../siasn_ref/diklat_tingkat.model");
    const refDiklat = require("../siasn_ref/diklat.model");

    return {
      peran: {
        modelClass: refPeran,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_master.rwyt_diklat.peran_id",
          to: "siasn_ref.ref_diklat_peran.peran_id",
        },
      },
      satuan: {
        modelClass: refSatuan,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_master.rwyt_diklat.satuan_id",
          to: "siasn_ref.ref_diklat_satuan.satuan_id",
        },
      },
      tingkat: {
        modelClass: refTingkat,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_master.rwyt_diklat.tingkat_id",
          to: "siasn_ref.ref_diklat_tingkat.tingkat_id",
        },
      },
      diklat: {
        modelClass: refDiklat,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_master.rwyt_diklat.jenis_diklat_id",
          to: "siasn_ref.ref_diklat.id",
        },
      },
    };
  }

  static get modifiers() {
    return {
      aktif(builder) {
        builder.where({ aktif: "Y" }).first;
      },
    };
  }
}

module.exports = RwytDiklat;
