const express = require("express");
const router = express.Router({ mergeParams: true });
const biodata = require("../../../../controllers/employees/personal/biodata.controller");

router.route("/").get(biodata.index);

module.exports = router;
