// cari berdasarkan nip
const biodataModel = require("../../../models/siasn_master/biodata.model");
const { raw } = require("objection");
const { detail } = require("../../../utils/detail-skpd");
const detailPangkat = require("../../../utils/detail-pangkat");
const isEmpty = require("lodash/isEmpty");

const get = async (req, res) => {
  try {
    const { nip } = req.params;
    const employee = await biodataModel
      .query()
      .where("nip_baru", nip)
      .select("pegawai_id", "nama", raw("nip_baru as nip"), "skpd_id")
      .withGraphFetched("[fileDiri(simple)]")
      .modify("simple")
      .first();

    // kalau gini terlalu berbahaya soalnya ini ga bisa di destructure
    if (!isEmpty(employee)) {
      const { pegawai_id } = employee;

      const currentPangkat = await detailPangkat(pegawai_id);

      const result = await detail(employee?.skpd_id);
      res.json({ ...employee, skpd: result?.detail, ...currentPangkat });
    } else {
      res.json({});
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({ code: 400, message: "Internal Server Error" });
  }
};

module.exports = { get };
