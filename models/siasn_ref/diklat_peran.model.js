const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class DiklatPeran extends Model {
  static get tableName() {
    return "siasn_ref.ref_diklat_peran";
  }

  static get idColumn() {
    return "siasn_ref.ref_diklat_peran.peran_id";
  }
}

module.exports = DiklatPeran;
