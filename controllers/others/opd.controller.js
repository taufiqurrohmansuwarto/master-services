const opdModel = require("../../models/siasn_ref/skpd.model");
const { employeeCounting } = require("../../utils/perangkat-daerah");

const counting = async (req, res) => {
  try {
    const pemprov = await opdModel
      .query()
      .where("pId", 1)
      .orderBy("name", "asc")
      .select("name", "id", "pId");

    // sekretaris daerah dengan kode 101
    const sekda = await opdModel
      .query()
      .where("pId", "10101")
      .orWhere("pId", "10102")
      .select("name", "id", "pId")
      .orderBy("name", "asc");

    const result = [...pemprov, ...sekda];
    const hasil = await employeeCounting(result?.map((r) => r?.id));

    const hasilAkhir = result?.map((r) => {
      const total = hasil?.find((h) => {
        const test = Object.keys(h).includes(r?.id);
        return test;
      })?.["count(*)"];
      return {
        ...r,
        total,
      };
    });

    res.json(hasilAkhir);
  } catch (error) {
    console.log(error);
    res.json({ code: 400, message: "internal server error" });
  }
};

module.exports = {
  counting,
};
