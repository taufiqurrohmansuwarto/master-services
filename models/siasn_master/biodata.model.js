const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class Biodata extends Model {
  static get tableName() {
    return "siasn_master.biodata";
  }

  static get idColumn() {
    return "pegawai_id";
  }

  static get relationMappings() {
    const fileDiri = require("./file_diri.model");
    const opd = require("../siasn_ref/skpd.model");
    const rwytPendidikan = require("../siasn_master/rwyt_pendidikan.model");
    const rwytDiklat = require("../siasn_master/rwyt_diklat.model");

    return {
      diklat: {
        modelClass: rwytDiklat,
        relation: Model.HasManyRelation,
        join: {
          from: "siasn_master.biodata.pegawai_id",
          to: "siasn_master.rwyt_diklat.pegawai_id",
        },
      },
      fileDiri: {
        modelClass: fileDiri,
        relation: Model.HasOneRelation,
        join: {
          from: "siasn_master.biodata.pegawai_id",
          to: "siasn_master.file_diri.pegawai_id",
        },
      },
      skpd: {
        modelClass: opd,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_master.biodata.skpd_id",
          to: "siasn_ref.skpd.id",
          pendidikan: {
            modelClass: rwytPendidikan,
            relation: Model.HasManyRelation,
            join: {
              from: "siasn_master.biodata.pegawai_id",
              to: "siasn_master.rwyt_pendidikan.pegawai_id",
            },
          },
        },
      },
    };
  }

  static get modifiers() {
    return {
      simple(builder) {
        builder
          .select("pegawai_id", "nip_baru", "skpd_id", "nama")
          .where("aktif", "Y")
          .andWhere("blokir", "N");
      },
      login(builder, username) {
        builder
          .select("nip_baru", "password", "pegawai_id")
          .where("nip_baru", username)
          .andWhere("aktif", "Y")
          .andWhere("blokir", "N")
          .first();
      },
    };
  }
}

module.exports = Biodata;
