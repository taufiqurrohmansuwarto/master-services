const jenisJabatanModel = require("../models/siasn_master/jenis_jabatan.model.js");
const refJenisJabatanModel = require("../models/siasn_ref/jenis_jabatan.model");

const rwytJabatanStruktural = require("../models/siasn_master/rwyt_jab_struktural.model");
const rwytJabatanFungsional = require("../models/siasn_master/rwyt_jab_fungsional.model");
const rwytJabatanPelaksana = require("../models/siasn_master/rwyt_jab_pelaksana.model");

const jenisJabatan = (data, jabatanId) => {
  return data?.find((d) => parseInt(d?.jabatan_id) === parseInt(jabatanId));
};

module.exports = async (userId) => {
  try {
    let data;
    const hasilPegawai = await jenisJabatanModel
      .query()
      .where("pegawai_id", userId)
      .first();

    const jabatan_id = hasilPegawai?.jabatan_id;

    const refJenisJabatan = await refJenisJabatanModel.query();
    const jabatan = jenisJabatan(refJenisJabatan, jabatan_id);

    if (jabatan_id) {
      if (parseInt(jabatan_id) === 1) {
        const result = await rwytJabatanStruktural
          .query()
          .where("pegawai_id", userId)
          .withGraphFetched("[jabatan_struktural,eselon]")
          .andWhere("aktif", "Y")
          .first();
        data = {
          is_eselon: true,
          eselon: `${result?.eselon?.eselon} - ${result?.eselon?.nama_eselon}`,
          jabatan: result?.jabatan_struktural?.jab_struktural,
          kode_jabatan: result?.jabatan_struktural?.jab_struktural_id,
          tmt_jab: result?.tmt_jab,
        };
      } else if (parseInt(jabatan_id) === 2) {
        const currentJabatanFungsional = await rwytJabatanFungsional
          .query()
          .select(
            "fungsional_id",
            "pegawai_id",
            "jft_id",
            "instansi",
            "unit_kerja",
            "induk",
            "tmt_jab"
          )
          .where("pegawai_id", userId)
          .withGraphFetched("jft(profile)")
          .andWhere("aktif", "Y")
          .first();
        data = {
          is_eselon: false,
          jabatan: `${currentJabatanFungsional?.jft?.name} ${currentJabatanFungsional?.jft?.jenjang_jab} (${currentJabatanFungsional?.jft?.gol_ruang})`,
          kode_jabatan: currentJabatanFungsional?.jft?.id,
          tmt_jab: currentJabatanFungsional?.tmt_jab,
        };
      } else if (parseFloat(jabatan_id) === 3) {
        const currentJabatanPelaksana = await rwytJabatanPelaksana
          .query()
          .select(
            "pegawai_id",
            "pelaksana_id",
            "jfu_id",
            "instansi",
            "unit_kerja",
            "induk",
            "tmt_jab"
          )
          .where("pegawai_id", userId)
          .withGraphFetched("jfu(profile)")
          .andWhere("aktif", "Y")
          .first();

        data = {
          is_eselon: false,
          jabatan: `${currentJabatanPelaksana?.jfu?.name}`,
          kode_jabatan: currentJabatanPelaksana?.jfu?.id,
          tmt_jab: currentJabatanPelaksana?.tmt_jab,
        };
      }

      return {
        jenis_jabatan: jabatan?.jenis_jabatan,
        kode_jenis_jabatan: jabatan_id,
        ...data,
      };
    }
  } catch (error) {
    console.log(error);
  }
};
