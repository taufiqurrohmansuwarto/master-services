const express = require("express");
const router = express.Router({ mergeParams: true });

router.use("/", require("./opd.route"));

module.exports = router;
