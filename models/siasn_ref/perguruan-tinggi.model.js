const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class PerguruanTinggi extends Model {
  static get tableName() {
    return "siasn_ref.ref_perguruan_tinggi";
  }

  static get idColumn() {
    return "pt_id";
  }
}

module.exports = PerguruanTinggi;
