const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class JenisJabatan extends Model {
  static get tableName() {
    return "siasn_ref.ref_jenis_jabatan";
  }

  static get idColumn() {
    return "siasn_ref.ref_jenis_jabatan.jabatan_id";
  }
}

module.exports = JenisJabatan;
