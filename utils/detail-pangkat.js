// untuk kebutuhan pangkat
const rwytPangkat = require("../models/siasn_master/rwyt_pangkat.model");

module.exports = async (pegawaiId) => {
  const pangkatPegawai = await rwytPangkat
    .query()
    .select("kp_id", "pegawai_id", "tmt_pangkat")
    .where("pegawai_id", pegawaiId)
    .andWhere("aktif", "Y")
    .withGraphJoined("pangkat(profile)")
    .first();

  let result = {};

  result = {
    ...result,
    pangkat: pangkatPegawai?.pangkat?.pangkat,
    golongan: pangkatPegawai?.pangkat?.gol_ruang,
    tmt_pangkat: pangkatPegawai?.tmt_pangkat,
  };

  return result;
};
