const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class RiwayatJabatanStruktural extends Model {
  static get tableName() {
    return "siasn_master.rwyt_jab_struktural";
  }

  static get idColumn() {
    return "struktural_id";
  }

  static get relationMappings() {
    const jabatanStruktural = require("../siasn_ref/jabatan_struktural.model");
    const eselon = require("../siasn_ref/eselon.model");

    return {
      jabatan: {
        relation: Model.BelongsToOneRelation,
        modelClass: jabatanStruktural,
        join: {
          from: "siasn_master.rwyt_jab_struktural.jab_struktural_id",
          to: "siasn_ref.ref_jabatan_struktural.jab_struktural_id",
        },
      },
      eselon: {
        relation: Model.BelongsToOneRelation,
        modelClass: eselon,
        join: {
          from: "siasn_master.rwyt_jab_struktural.eselon_id",
          to: "siasn_ref.ref_eselon.eselon_id",
        },
      },
    };
  }
}

module.exports = RiwayatJabatanStruktural;
