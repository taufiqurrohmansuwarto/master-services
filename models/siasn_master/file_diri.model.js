const { Model, raw } = require("objection");
const knex = require("../../knex");
const url = process.env.URL_FILE;

Model.knex(knex);

class FileDiri extends Model {
  static get tableName() {
    return "siasn_master.file_diri";
  }

  static get idColumn() {
    return "siasn_master.file_diri.file_id";
  }

  static get modifiers() {
    return {
      simple(builder) {
        builder.select(
          // "file_id",
          // "pegawai_id",
          raw(
            `concat('https://master.bkd.jatimprov.go.id/files_jatimprov/', ??)`,
            "file_foto"
          ).as("foto")
        );
      },
    };
  }
}

module.exports = FileDiri;
