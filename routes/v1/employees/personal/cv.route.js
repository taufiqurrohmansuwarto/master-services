const express = require("express");
const router = express.Router({ mergeParams: true });
const controller = require("../../../../controllers/employees/cv.controller");

router.route("/").get(controller.get);

module.exports = router;
