const express = require("express");
const router = express.Router();

router.use("/jenjang", require("./jenjang.route"));
router.use("/pekerjaan-anak", require("./pekerjaan-anak.route"));

module.exports = router;
