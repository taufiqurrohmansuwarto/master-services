const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class Prodi extends Model {
  static get tableName() {
    return "siasn_ref.ref_prodi";
  }

  static get idColumn() {
    return "prodi_id";
  }
}

module.exports = Prodi;
