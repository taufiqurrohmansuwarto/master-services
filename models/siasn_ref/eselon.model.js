const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class Eselon extends Model {
  static get tableName() {
    return "siasn_ref.ref_eselon";
  }

  static get idColumn() {
    return "eselon_id";
  }
}

module.exports = Eselon;
