const jenisJabatanModel = require("../../../models/siasn_master/jenis_jabatan.model");

// biodata
const biodata = require("../../../models/siasn_master/biodata.model");

const fungsionalModel = require("../../../models/siasn_master/rwyt_jab_fungsional.model");
const strukturalModel = require("../../../models/siasn_master/rwyt_jab_struktural.model");
const pelaksanaModel = require("../../../models/siasn_master/rwyt_jab_pelaksana.model");

const pangkatModel = require("../../../models/siasn_master/rwyt_pangkat.model");

module.exports.get = async (req, res) => {
  const { employeeId } = req.params;

  const { nama, nip_baru } = await biodata
    .query()
    .findById(employeeId)
    .andWhere("aktif", "Y")
    .andWhere("blokir", "N");

  const { jabatan_id } = await jenisJabatanModel
    .query()
    .where("pegawai_id", employeeId)
    .select("jabatan_id")
    .first();

  if (!jabatan_id || !nama) {
    res.boom.notFound("jabatan atau nama yang dicari tidak ditemukan");
  } else if (jabatan_id === "1") {
    const {
      induk,
      instansi,
      unit_kerja,
      struktural_id,
      jabatan: { jab_struktural },
    } = await strukturalModel
      .query()
      .where("pegawai_id", employeeId)
      .andWhere("aktif", "Y")
      .first()
      .withGraphFetched("[jabatan]");

    const {
      pangkat: { gol_ruang, pangkat: pangkatPegawai },
    } = await pangkatModel
      .query()
      .where("pegawai_id", employeeId)
      .first()
      .andWhere("aktif", "Y")
      .withGraphFetched("[pangkat]");

    const data = {
      id: employeeId,
      nama,
      nip: nip_baru,
      struktural_id,
      induk,
      instansi,
      jabatan: jab_struktural,
      golongan: gol_ruang,
      pangkat: pangkatPegawai,
    };

    res.json({ data });
  } else if (jabatan_id === "2") {
    const {
      fungsional_id,
      induk,
      instansi,
      jabatan: { gol_ruang, jenjang_jab, name },
    } = await fungsionalModel
      .query()
      .where("pegawai_id", employeeId)
      .andWhere("aktif", "Y")
      .first()
      .withGraphFetched("[jabatan]");

    const data = {
      id: employeeId,
      nama,
      nip: nip_baru,
      fungsional_id,
      induk,
      instansi,
      jabatan: name,
      golongan: gol_ruang,
      pangkat: jenjang_jab,
    };

    res.json({ data });
  } else if (jabatan_id === "3") {
    const {
      jfu_id,
      pelaksana_id,
      induk,
      instansi,
      jabatan: { name },
    } = await pelaksanaModel
      .query()
      .where("pegawai_id", employeeId)
      .select("jfu_id", "pelaksana_id", "induk", "instansi")
      .andWhere("aktif", "Y")
      .first()
      .withGraphFetched("[jabatan(profile)]");

    // pangkat dan golongan
    const {
      pangkat: { gol_ruang, pangkat: pangkatPegawai },
    } = await pangkatModel
      .query()
      .where("pegawai_id", employeeId)
      .first()
      .andWhere("aktif", "Y")
      .withGraphFetched("[pangkat]");

    const data = {
      id: employeeId,
      nama,
      nip: nip_baru,
      jfu_id,
      pelaksana_id,
      induk,
      instansi,
      jabatan: name,
      golongan: gol_ruang,
      pangkat: pangkatPegawai,
    };

    res.json({ data });
  } else {
    res.json({ data: jabatan_id });
  }
};
