const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class JabatanStruktural extends Model {
  static get tableName() {
    return "siasn_ref.ref_jabatan_struktural";
  }

  static get modifiers() {
    return {
      profile(builder) {
        builder.select("jab_struktural_id", "jab_struktural");
      },
    };
  }

  static get idColumn() {
    return "jab_struktural_id";
  }
}

module.exports = JabatanStruktural;
