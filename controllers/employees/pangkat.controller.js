const model = require("../../models/siasn_master/rwyt_pangkat.model");

const list = async (req, res) => {
  try {
    const { employeeId } = req.params;
    const result = await model.query().where("pegawai_id", employeeId);

    res.json({ data: result });
  } catch (error) {
    res.boom.notFound("tidak ada error");
  }
};

const get = async (req, res) => {};
const patch = async (req, res) => {};
const put = async (req, res) => {};
const remove = async (req, res) => {};

module.exports = {
  list,
  get,
  patch,
  put,
  remove,
};
