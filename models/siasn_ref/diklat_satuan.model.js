const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class DiklatSatuan extends Model {
  static get tableName() {
    return "siasn_ref.ref_diklat_satuan";
  }

  static get idColumn() {
    return "siasn_ref.ref_diklat_satuan.satuan_id";
  }
}

module.exports = DiklatSatuan;
