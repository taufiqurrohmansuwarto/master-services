const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class JenisJabatanPegawai extends Model {
  static get tableName() {
    return "siasn_master.jenis_jabatan";
  }

  static get idColumn() {
    return "jenis_jab_id";
  }
}

module.exports = JenisJabatanPegawai;
