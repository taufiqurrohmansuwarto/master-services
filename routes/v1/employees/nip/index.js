const express = require("express");
const router = express.Router({ mergeParams: true });
const biodataControler = require("../../../../controllers/employees/nip/biodata.controller");

router.route("/biodata").get(biodataControler.get);

module.exports = router;
