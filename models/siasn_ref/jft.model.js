const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class JabatanFungsionalTerapan extends Model {
  static get tableName() {
    return "siasn_ref.ref_jft";
  }

  static get modifiers() {
    return {
      profile(builder) {
        builder.select(
          "id",
          "name",
          "tingkat",
          "jenjang_jab",
          "gol_ruang",
          "kelas_jab"
        );
      },
    };
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = JabatanFungsionalTerapan;
