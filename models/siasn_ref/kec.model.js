const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class Kecamatam extends Model {
  static get tableName() {
    return "siasn_ref.kec";
  }

  static get idColumn() {
    return "siasn_ref.id_kec";
  }

  static get relationMappings() {
    const provinsi = require("./prov.model");
    const kabupaten = require("./kabkot.model");

    return {
      provinsi: {
        modelClass: provinsi,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_ref.kec.id_prov",
          to: "siasn_ref.prov.id_prov",
        },
      },
      kabupaten: {
        modelClass: kabupaten,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_ref.kec.id_kabkot",
          to: "siasn_ref.kabkot.id_kabkot",
        },
      },
    };
  }
}

module.exports = Kecamatam;
