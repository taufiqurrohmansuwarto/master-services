const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class JenisKepegawaian extends Model {
  static get tableName() {
    return "siasn_master.rwyt_jenis_kepegawaian";
  }

  static get idColumn() {
    return "jenis_kepegawaian_id";
  }
}

module.exports = JenisKepegawaian;
