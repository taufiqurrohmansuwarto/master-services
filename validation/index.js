const Joi = require("joi");
const validator = require("express-joi-validation").createValidator({});

const arraySchema = Joi.object({
  pegawai_id: Joi.array().allow(null).max(20).min(0),
});

module.exports = {
  arrayValidation: validator.query(arraySchema),
};
