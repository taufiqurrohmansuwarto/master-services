const model = require("../../models/siasn_master/biodata.model");
const _ = require("lodash");

const list = async (req, res) => {
  try {
    const { pegawai_id } = req.query;
    console.log(pegawai_id);
    const result = await model
      .query()
      .whereIn("pegawai_id", pegawai_id)
      .withGraphFetched("[fileDiri(simple)]")
      .select("pegawai_id", "nip_baru", "skpd_id", "nama", "no_hp", "email");
    // .modify("simple");

    const data = _.chain(result)
      .map((e) => {
        const { fileDiri, ...data } = e;
        return {
          ...data,
          file_foto: fileDiri?.foto,
        };
      })
      .value();

    res.json({ data });
  } catch (error) {
    console.log(error);
    res.boom.notFound();
  }
};

const get = async (req, res) => {};
const patch = async (req, res) => {};
const put = async (req, res) => {};
const remove = async (req, res) => {};

module.exports = {
  list,
  get,
  patch,
  put,
  remove,
};
