const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class DiklatSatuan extends Model {
  static get tableName() {
    return "siasn_ref.ref_diklat_tingkat";
  }

  static get idColumn() {
    return "siasn_ref.ref_diklat_tingkat.tingkat_id";
  }
}

module.exports = DiklatSatuan;
