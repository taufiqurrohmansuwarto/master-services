const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class Pangkat extends Model {
  static get tableName() {
    return "siasn_ref.ref_pangkat";
  }

  static get modifiers() {
    return {
      profile(builder) {
        builder.select("pangkat_id", "gol_ruang", "pangkat");
      },
    };
  }

  static get idColumn() {
    return "pangkat_id";
  }
}

module.exports = Pangkat;
