const express = require("express");
const router = express.Router({ mergeParams: true });
const jabatanController = require("../../../controllers/employees/jabatan.controller");

router.route("/").get(jabatanController.list);

module.exports = router;
