const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class Diklat extends Model {
  static get tableName() {
    return "siasn_ref.ref_diklat";
  }

  static get idColumn() {
    return "siasn_ref.ref_diklat.id";
  }
}

module.exports = Diklat;
