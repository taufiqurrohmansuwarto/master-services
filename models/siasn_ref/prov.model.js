const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class Provinsi extends Model {
  static get tableName() {
    return "siasn_ref.prov";
  }

  static get idColumn() {
    return "siasn_ref.id_prov";
  }
}

module.exports = Provinsi;
