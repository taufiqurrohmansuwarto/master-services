const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class KedudukanHukum extends Model {
  static get tableName() {
    return "siasn_master.rwyt_kedudukan_hukum";
  }

  static get idColumn() {
    return "kedudukan_hukum_id";
  }
}

module.exports = KedudukanHukum;
