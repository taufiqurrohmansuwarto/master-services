const express = require("express");
const router = express.Router({ mergeParams: true });
const pendidikanController = require("../../../controllers/employees/pendidikan.controller");

router.route("/").get(pendidikanController.list);

module.exports = router;
