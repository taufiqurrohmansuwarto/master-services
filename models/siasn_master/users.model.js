const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class Biodata extends Model {
  static get tableName() {
    return "siasn_master.users";
  }

  static get idColumn() {
    return "username";
  }

  static get modifiers() {
    return {
      aktif(builder, username) {
        builder
          .where("username", username)
          .andWhere("aktif", "Y")
          .andWhere("blokir", "N")
          .first();
      },
    };
  }
}

module.exports = Biodata;
