const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class RiwayatJabatanPelaksana extends Model {
  static get tableName() {
    return "siasn_master.rwyt_jab_pelaksana";
  }

  static get idColumn() {
    return "pelaksana_id";
  }

  static get relationMappings() {
    const jfu = require("../siasn_ref/jfu.model");
    return {
      jfu: {
        relation: Model.BelongsToOneRelation,
        modelClass: jfu,
        join: {
          from: "siasn_master.rwyt_jab_pelaksana.jfu_id",
          to: "siasn_ref.ref_jfu.id",
        },
      },
    };
  }
}

module.exports = RiwayatJabatanPelaksana;
