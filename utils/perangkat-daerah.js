const biodataModel = require("../models/siasn_master/biodata.model");

const employeeCounting = async (listId) => {
  try {
    let iput = [];
    listId?.forEach((id) => {
      const hasil = biodataModel
        .query()
        .select(parseInt(id))
        .count()
        .where("skpd_id", "like", `${id}%`)
        .first();
      iput.push(hasil);
    });
    const result = await Promise.all(iput);
    return result;
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  employeeCounting,
};
