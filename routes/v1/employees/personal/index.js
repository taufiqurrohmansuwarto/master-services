const express = require("express");
const router = express.Router({ mergeParams: true });

router.use("/pendidikan", require("./pendidikan.route"));
router.use("/jabatan", require("./jabatan.route"));
router.use("/biodata", require("./biodata.route"));

router.use("/curiculum-vitae", require("./cv.route"));

module.exports = router;
