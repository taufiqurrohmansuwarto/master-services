const Joi = require("joi");

// params
const params = Joi.object({
  id: Joi.number().integer().required(),
});

module.exports = { params };
