const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class RiwayatPendidikan extends Model {
  static get tableName() {
    return "siasn_master.rwyt_pendidikan";
  }

  static get idColumn() {
    return "siasn_master.rwyt_pendidikan.pendidikan_id";
  }

  static get modifiers() {
    return {
      aktif(builder) {
        builder.where({ aktif: "Y" }).first();
      },
    };
  }

  static get relationMappings() {
    const jenjang = require("../siasn_ref/jenjang.model");
    const pt = require("../siasn_ref/perguruan-tinggi.model");
    const prodi = require("../siasn_ref/prodi.model");

    return {
      prodi: {
        modelClass: prodi,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_master.rwyt_pendidikan.prodi_id",
          to: "siasn_ref.ref_prodi.prodi_id",
        },
      },
      jenjang: {
        modelClass: jenjang,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_master.rwyt_pendidikan.jenjang_id",
          to: "siasn_ref.ref_jenjang.jenjang_id",
        },
      },
      perguruan_tinggi: {
        modelClass: pt,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_master.rwyt_pendidikan.pt_id",
          to: "siasn_ref.ref_perguruan_tinggi.pt_id",
        },
      },
    };
  }
}

module.exports = RiwayatPendidikan;
