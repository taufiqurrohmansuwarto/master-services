const { Model } = require("objection");
const knex = require("../../knex");

Model.knex(knex);

class Skpd extends Model {
  static get tableName() {
    return "siasn_ref.skpd";
  }

  static get idColumn() {
    return "siasn_ref.skpd.id";
  }

  static get modifiers() {}
}

module.exports = Skpd;
