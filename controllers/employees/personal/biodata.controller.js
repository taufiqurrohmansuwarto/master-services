const isEmpty = require("lodash/isEmpty");
const { raw } = require("objection");
const model = require("../../../models/siasn_master/biodata.model");
const detailPangkat = require("../../../utils/detail-pangkat");
const { detail } = require("../../../utils/detail-skpd");

const index = async (req, res) => {
  try {
    const { employeeId } = req.params;
    const employee = await model
      .query()
      .findById(employeeId)
      .withGraphFetched("[fileDiri(simple)]")
      .modify("simple")
      .select(
        "pegawai_id",
        "nama",
        raw("nip_baru as nip"),
        "skpd_id",
        "nik",
        "no_hp",
        "email"
      );
    if (!isEmpty(employee)) {
      const { pegawai_id } = employee;

      const currentPangkat = await detailPangkat(pegawai_id);

      const result = await detail(employee?.skpd_id);
      res.json({ ...employee, skpd: result?.detail, ...currentPangkat });
    } else {
      res.json({});
    }
  } catch (error) {
    console.log(error);
    res.boom.badRequest("Internal Server Error");
  }
};

module.exports = {
  index,
};
