const detailJabatan = require("../../utils/detail-jabatan");
const biodata = require("../../models/siasn_master/biodata.model");

const list = async (req, res) => {
  try {
    const { employeeId } = req.params;
    const data = await detailJabatan(employeeId);
    const employeeProfile = await biodata
      .query()
      .findById(employeeId)
      .select("nip_baru", "nama")
      .withGraphFetched("[skpd]");

    if (data) {
      res.json({ ...data, ...employeeProfile });
    } else {
      res.boom.badRequest("id jabatan belum disetting");
    }
  } catch (error) {
    console.log(error);
    res.boom.notFound("tidak ada error");
  }
};

const get = async (req, res) => {};
const patch = async (req, res) => {};
const put = async (req, res) => {};
const remove = async (req, res) => {};

module.exports = {
  list,
  get,
  patch,
  put,
  remove,
};
