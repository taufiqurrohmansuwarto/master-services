const express = require("express");
const router = express.Router({ mergeParams: true });
const opdController = require("../../../controllers/others/opd.controller");

router.route("/count").get(opdController.counting);

module.exports = router;
