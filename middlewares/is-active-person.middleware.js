const biodataModel = require("../models/siasn_master/biodata.model");
const userModel = require("../models/siasn_master/users.model");

const employeeActive = async (req, res, next) => {
  const { id } = req.params;
  const result = await biodataModel
    .query()
    .findById(id)
    .andWhere("aktif", "Y")
    .andWhere("blokir", "N")
    .first();

  if (!result) {
    res.boom.notFound("Employee is no longer active or not found");
  }

  next();
};

const userActive = async (req, res, next) => {
  const { username } = req.params;
  const result = await userModel.query().findById(username);
  if (!result) {
    res.boom.notFound("User is no longer active or not found");
  }

  next();
};

module.exports = {
  employeeActive,
  userActive,
};
