const express = require("express");
const router = express.Router({ mergeParams: true });
const employeeController = require("../../../controllers/employees/employees.controller");
const { arrayValidation } = require("../../../validation");

router.route("/").get(arrayValidation, employeeController.list);
router.use("/:employeeId", require("./personal"));
router.use("/nip/:nip", require("./nip"));

module.exports = router;
