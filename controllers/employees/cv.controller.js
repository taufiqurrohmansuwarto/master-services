const biodata = require("../../models/siasn_master/biodata.model");
const detailJabatan = require("../../utils/detail-jabatan");
const detailOpd = require("../../utils/detail-opd");
const detailPangkat = require("../../utils/detail-pangkat");
const { lastEducation } = require("../../utils/utils");

module.exports.get = async (req, res) => {
  try {
    const { employeeId } = req.params;
    const result = await biodata
      .query()
      .findById(employeeId)
      .andWhere("aktif", "Y")
      .andWhere("blokir", "N")
      .withGraphFetched(
        "[pendidikan(aktif).[prodi,jenjang,perguruan_tinggi], diklat(aktif).[peran, satuan,tingkat,diklat]]"
      )
      .select("pegawai_id", "nama", "nip_baru", "skpd_id");

    // kalau ga ada ngapain

    if (!result) {
      res.boom.notFound("Employee not found!");
    } else {
      const jabatan = await detailJabatan(employeeId);
      const pangkat = await detailPangkat(employeeId);
      const detailPerangkatDaerah = await detailOpd(result?.skpd_id);
      const detailPendidikan = result?.pendidikan?.length
        ? lastEducation(result?.pendidikan[0])
        : {};

      const { pendidikan, ...lastResult } = result;
      res.json({
        ...lastResult,
        jabatan,
        pangkat,
        perangkat_daerah: detailPerangkatDaerah,
        pendidikan: detailPendidikan,
      });
    }
  } catch (error) {
    console.log(error);
    res.boom.badRequest("Internal Server Error");
  }
};
