const express = require("express");
const router = express.Router({ mergeParams: true });

router.use("/pendidikan", require("./pendidikan.route"));
router.use("/jabatan", require("./jabatan.route"));
router.use("/", require("./employees.route"));

module.exports = router;
