const filterPendidikan = (dataPendidikan) => {
  let data;

  if (!dataPendidikan.length) {
    data = [];
  } else {
    data = dataPendidikan?.map((d) => {});
  }
  return data;
};
const filterJabatan = (data) => {
  return data;
};
const filterPangkat = (data) => {
  return data;
};

const lastEducation = (data) => {
  const {
    jenjang_id,
    jenjang,
    perguruan_tinggi,
    fakultas_pt,
    akreditasi_pt,
    thn_lulus_pt,
    no_ijazah_pt,
    tgl_ijazah_pt,
    ipk_pt,
    // sma
    nama_sekolah_sma,
    jurusan_sma,
    akreditasi_sma,
    thn_lulus_sma,
    no_ijazah_sma,
    tgl_ijazah_sma,
    nilai_akhir_sma,
    nilai_un_sma,
    file_ijazah,
    file_nilai,
    tgl_edit,
    jam_edit,
  } = data;

  const file_ijazah_url = process.env.URL_FILE_MASTER + file_ijazah;
  const file_nilai_url = process.env.URL_FILE_MASTER + file_nilai;

  let pendidikanAkhir = {};
  if (!data) {
    pendidikanAkhir = {};
  }
  if (jenjang_id > 3) {
    pendidikanAkhir = {
      kode_jenjang: jenjang_id,
      ...pendidikanAkhir,
      jenjang: jenjang?.jenjang_pendidikan,
      nama_sekolah: perguruan_tinggi?.perguruan_tinggi,
      akreditasi: akreditasi_pt,
      fakultas: fakultas_pt,
      tahun_lulus: thn_lulus_pt,
      no_ijazah: no_ijazah_pt,
      tgl_ijazah: tgl_ijazah_pt,
      ipk: ipk_pt,
      file_ijazah_url,
      file_nilai_url,
      tgl_edit,
      jam_edit,
    };
  } else {
    pendidikanAkhir = {
      kode_jenjang: jenjang_id,
      ...pendidikanAkhir,
      jenjang: jenjang?.jenjang_pendidikan,
      nama_sekolah: nama_sekolah_sma,
      akreditasi: akreditasi_sma,
      jurusan: jurusan_sma,
      tahun_lulus: thn_lulus_sma,
      no_ijazah: no_ijazah_sma,
      tgl_ijazah: tgl_ijazah_sma,
      nilai_akhir: nilai_akhir_sma,
      nilai_un: nilai_un_sma,
      file_ijazah_url,
      file_nilai_url,
      tgl_edit,
      jam_edit,
    };
  }

  return pendidikanAkhir;
};

module.exports = {
  filterPendidikan,
  filterJabatan,
  filterPangkat,
  lastEducation,
};
