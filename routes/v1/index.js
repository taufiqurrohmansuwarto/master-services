const express = require("express");
const router = express.Router({ mergeParams: true });

// fasilitator route
router.use("/employees", require("./employees"));
router.use("/opd", require("./others/opd.route"));

module.exports = router;
